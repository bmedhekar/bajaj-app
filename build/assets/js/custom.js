function set_bg() {

    $('.set-bg').each(function () {
        $(this).css('background-image', 'url(' + $(this).attr('data-img') + ')');
        $(this).css('background-image', 'cover');
    });

}

function thumbGallery() {

    $(".thumb-images img").click(function () {
        var currentElm = $(this);
        var imagePath = currentElm.attr("data-src");
        var parentWrap = currentElm.closest(".bs-gallery");
        thumbSwiper.stopAutoplay();
        parentWrap.find(".big-image .img-wrap-app img").attr("src", imagePath);
    });

}

function timeCounter(element) {

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yy = today.getFullYear();

    var dateandtime = yy + "/" + mm + "/" + dd + " 00:00:00";
    $(element).countdowntimer({
        dateAndTime: dateandtime,
        labelsFormat: true,
        displayFormat: "HMS",
    });

}


/* home page - video jquery  */
var playerElement;
var last_play = '';

function createVideoPlayer() {

    $(".name-video-wrap .video-wrap").each(function () {
        var videoWrap = $(this);
        playerElement = videoWrap.find(".player");
        $(".name-video-wrap .playbtn").attr("disabled", true);
        $(".name-video-wrap .playbtn").addClass("disabled");
        playerElement.YTPlayer({
            videoURL: videoWrap.find(".player").attr('data-id')
        });
        var url = "https://img.youtube.com/vi/" + videoWrap.find(".player").attr('data-id') + "/0.jpg";
        $(this).css('background', "url(" + url + ")");
        $(this).css('background-size', "cover");
        $(this).css('background-position', "center");
    });

    $(".name-video-wrap .playbtn").click(function () {
        if (!$(this).attr("disabled")) {
            var parentWrap = $(this).closest(".name-video-wrap");
            last_play = parentWrap.find(".player");
            parentWrap.find(".player").YTPPlay();
            $(this).fadeOut();
            parentWrap.find(".name-wrap").fadeOut();
        }
    });
    $(".name-video-wrap .video-wrap .player").on("YTPReady", function (e) {
        $(".name-video-wrap .playbtn").attr("disabled", false);
        $(".name-video-wrap .playbtn").removeClass("disabled");
    });
    $(".name-video-wrap .video-wrap .player").on("YTPEnd, YTPPause", function (e) {
        $(".name-wrap").fadeIn();
        $(".video-wrap .playbtn").fadeIn();
    });

}
var delayToOpen;

function homeGridProductClick() {

    $(".home-category-slider .product-detail").click(function () {
        var showData = $(this).attr("data-show");
        var parentRow = $(this).closest(".grid-row").attr("data-row");
        $("body").addClass("disable-scroll");
        $(".home-category-slider .product-detail").removeClass("active");
        $(this).addClass("active");
        $(".bs-home-category-app").addClass("clicked");

        var checkPopupOpened = $(".bs-home-category-detail-app").hasClass("active");
        //console.log(checkPopupOpened);

        if (checkPopupOpened) {
            //delayToOpen = 1800; keep for fold effect
            delayToOpen = 500; // remove this
            setTimeout(function () {
                $(".bs-home-category-detail-app .product-detail-popup-app").removeClass("active");
                //}, 1600); keep for fold effect
            }, 400); // remove this
        } else {
            delayToOpen = 150;
        }

        $(".bs-home-category-detail-app .product-detail-popup-app").removeClass("add-shadow");


        //var changeTime;

        if ($(".bs-home-category-detail-app").hasClass("active")) {
            $(".bs-home-category-detail-app .product-detail-popup-app.active").addClass("close-popup");
            $(".bs-home-category-detail-app").addClass("hide-close");
            //changeTime = 1900;
        } else {
            $(".bs-home-category-detail-app").addClass("active");
        }
        var fetchHeight = $(".bs-home-category-detail-app #" + parentRow + " .product-detail-popup-app[data-content='" + showData + "']").height();
        setTimeout(function () {
            $(".bs-home-category-detail-app .product-detail-popup-app").removeClass("close-popup");
            $(".bs-home-category-detail-app #" + parentRow + " .product-detail-popup-app[data-content='" + showData + "']").addClass("active");
            $(".bs-home-category-detail-app").height(fetchHeight);
            $(".bs-home-category-detail-app").removeClass("hide-close");
        }, delayToOpen);

        setTimeout(function () {
            $(".bs-home-category-detail-app #" + parentRow + " .product-detail-popup-app[data-content='" + showData + "']").addClass("add-shadow");
        }, 1000);

        setTimeout(function () {
            $(".bs-home-category-app").removeClass("clicked");
        }, 1200);

        if (!last_play == "") {
            last_play.YTPStop();
        }
    });

    $(".bs-home-category-detail-app .close").click(function () {
        $("body").removeClass("disable-scroll");
        if (!last_play == "") {
            last_play.YTPStop();
            last_play = '';
        }

        rescentClick.startAutoplay();
        console.log($(".home-category-slider").find(".product-detail.active"), prevslider);

        $(".bs-home-category-detail-app .product-detail-popup-app").removeClass("add-shadow");
        $(".bs-home-category-detail-app .product-detail-popup-app.active").addClass("close-popup");
        setTimeout(function () {
            $(".bs-home-category-detail-app .product-detail-popup-app").removeClass("close-popup");
            $(".bs-home-category-detail-app .product-detail-popup-app").removeClass("active");
            $(".bs-home-category-detail-app, .home-category-slider .product-detail").removeClass("active");
        }, 100);
    });

}

/* home page grid slider */
var homeSwiperVar,
    prevslider, rescentClick;
var autoPlaySliderTime;
var arr = new Array();
var randValue, newRandValue;
var sliderIndex = 1;

function homeCategorySlider() {


    var parentRow, nextBtn, prevBtn;
    if ($(".home-category-slider").length) {

        var i = $(".home-category-slider").length;

        for (var a = 1; a <= i; a++) {
            randValue = 2500 + (a * 2000);
            arr.push(randValue);
        }
        var timeToWait = i * 3500;

        $(".home-category-slider").each(function (e) {
            var currentElm = $(this);
            //console.log(arr);
            parentRow = currentElm.closest(".grid-row").attr("data-row");
            nextBtn = ".grid-row[data-row='" + parentRow + "'] .swiper-button-next";
            prevBtn = ".grid-row[data-row='" + parentRow + "'] .swiper-button-prev";

            var stopSlider = function () {
                newSliderVar.stopAutoplay();
                if (rescentClick && rescentClick != newSliderVar) {
                    rescentClick.startAutoplay();
                }
                rescentClick = newSliderVar;
            }

            var waitForMyTurn = function () {
                //newSliderVar.stopAutoplay();
                newRandValue = 200;

                // setTimeout(function () {

                //     newSliderVar.startAutoplay();
                // }, timeToWait);
            }

            newRandValue = arr.splice(Math.floor(Math.random() * (i - e)), 1)[0];

            var newSliderVar = "homeSwiperVar-" + sliderIndex;
            //console.log("00--" + newSliderVar);
            newSliderVar = new Swiper(".grid-row[data-row='" + parentRow + "'] .home-category-slider", {
                nextButton: nextBtn,
                prevButton: prevBtn,
                slidesPerView: "auto",
                speed: 800,
                autoplay: newRandValue,
                slidesPerGroup: 1,
                spaceBetween: 0,
                initialSlide: 0,
                centeredSlides: false,
                autoplayDisableOnInteraction: true,
                onClick: stopSlider,
                onSlideChangeEnd: function () {
                    newSliderVar.stopAutoplay();
                    setTimeout(function () {
                        console.log("start");
                        newSliderVar.startAutoplay();
                    }, timeToWait);

                }
            });

            randValue = 0;
            autoPlaySliderTime = 0;

            currentElm.mouseenter(function () {
                newSliderVar.stopAutoplay();
            });

            currentElm.mouseleave(function () {
                newSliderVar.startAutoplay();
                var checkActive = currentElm.find(".product-detail.active");
                if (checkActive.length) {
                    newSliderVar.stopAutoplay();
                }
            });

            sliderIndex++;
        });
    }

}

function homeProductCarouselSwiper() {

    $(".product-detail-popup-app .product-slider").each(function () {
        parentPopupWrap = $(this).closest(".product-detail-popup-app").attr("data-content");
        nextBtn = ".product-detail-popup-app[data-content='" + parentPopupWrap + "'] .swiper-button-next";
        prevBtn = ".product-detail-popup-app[data-content='" + parentPopupWrap + "'] .swiper-button-prev";

        homeProductCarouselSwiper = new Swiper(".product-slider .swiper-container", {
            nextButton: nextBtn,
            prevButton: prevBtn,
            slidesPerView: 1,
            speed: 600,
            slidesPerColumn: 2,
            //slidesPerGroup: 1,
            spaceBetween: 0,
            initialSlide: 0,
            centeredSlides: true,
            pagination: '.swiper-pagination',
            paginationType: 'fraction',
            paginationFractionRender: function (swiper, currentClassName, totalClassName) {
                return '<span class="' + currentClassName + '"></span>' +
                    ' of ' +
                    '<span class="' + totalClassName + '"></span>';
            }
        });
    });
}

var thumbSwiper;

function thumbGallerySlider() {
    try {
        thumbSwiper = $(".thumb-images .swiper-container").swiper({
            slidesPerView: 2,
            speed: 600,
            autoplay: 4000,
            spaceBetween: 8,
            autoplayDisableOnInteraction: true
        });
    } catch (err) {
        console.log(err);
    }
}

function flipSwiperSlider() {
    try {
        $(".flip-slider .swiper-container").swiper({
            slidesPerView: 1,
            speed: 600,
            centeredSlides: true,
            effect: 'coverflow',
            coverflow: {
                rotate: 60,
                stretch: 0,
                depth: 600,
                modifier: 1,
                slideShadows: false
            }
        });
    } catch (err) {
        console.log(err);
    }
}

$(document).ready(function () {



    $("#today-deal").countdowntimer({
        dateAndTime: "2019/11/04/ 00:00:00",
        labelsFormat: true,
        displayFormat: "HMS"
    });



    flipSwiperSlider();
    //timeCounter("#today-deal")
    set_bg();
    thumbGallerySlider();
    thumbGallery();
    homeCategorySlider();
    homeProductCarouselSwiper();
    homeGridProductClick();
});