module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        ssi: {
            options: {
                input: '',
                output: 'build/',
                matcher: '*.shtml'
            }
        },
        sass: {
            dist: {
                options: {
                    // style: 'compressed',
                    precision: 2
                },
                files: {
                    'assets/css/custom.css': 'assets/sass/custom.scss'
                }
            }
        },
        // ssi: {
        //     options: {
        //         ext: '.html'
        //     },
        //     files: [{    
        //         expand: true,
        //         cwd: '/',
        //         src: ['**/*.shtml'],
        //         dest: '/',
        //     }]
        // },
        jshint: {
            files: {
                src: ['assets/js/**/*.js']
            }
        },
        // useminPrepare: {
        //     html: {
        //         src: ['templates/*.html']
        //     },
        //     options: {
        //         dest: 'build/'
        //     }
        // },
        cssmin: {
            target: {
                files: {
                    'assets/css/libraries.css': ['assets/css/libraries/**/*.css'],
                    //,'assets/css/custom.css': ['assets/css/custom.css']
                }
            }
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src: [
                        'assets/css/*.css',
                        '*.shtml',
                        '*.html'
                    ]
                },
                options: {
                    watchTask: true,
                    proxy: "http://localhost/"
                    // proxy: "http://all.loc/"
                }
            }
        },
        tinypng: {
            options: {
                apiKey: "7_yk9teC5pMwn3LgDIHp7gwG0RGeb0Z5",
                summarize: true
            },
            compress: {
                expand: true,
                src: 'assets/**/*.png',
                dest: './images/',
                ext: '.png'
            },
            compress2: {
                expand: true,
                src: 'assets/**/*.jpg',
                dest: './images/',
                ext: '.jpg'
            },
        },
        uglify: {
            options: {
                mangle: false,
                beautify: true,
                compress: false
            },
            my_target: {
                files: {
                    'assets/js/vendor.js': [
                        'assets/js/libraries/jquery.js',
                        'assets/js/libraries/modernizr.js',
                        'assets/js/libraries/detectizr.js',
                        'assets/js/libraries/swiper.min.js',
                        'assets/js/libraries/countdownTimer.min.js'
                    ]
                }
            }
        },
        usemin: {
            html: ['build/*.html']
        },
        copy: {
            // html: {
            //     expand: true,
            //     cwd: 'build-html/',
            //     src: ['*.html'],
            //     dest: 'build/'
            // },
            shtml: {
                files: [{
                    expand: true,
                    dot: true,
                    src: ['build/*.shtml'],
                    dest: [''],
                    rename: function (dest, src) {
                        return dest + src.replace(/\.shtml$/, ".html");
                    }
                }]
            },
            images: {
                expand: true,
                cwd: 'assets/images/',
                src: '**',
                dest: 'build/assets/images/'
            },
            fonts: {
                expand: true,
                cwd: 'assets/fonts/',
                src: '**',
                dest: 'build/assets/fonts/'

            },
            css: {
                expand: true,
                cwd: 'assets/css',
                src: '*.css',
                dest: 'build/assets/css/'

            },
            js: {
                expand: true,
                cwd: 'assets/js',
                src: '*.js',
                dest: 'build/assets/js/'
            }
        },
        clean: {
            build: {
                src: ['build/*.shtml']
            }
        },
        watch: {
            grunt: {
                files: ['Gruntfile.js']
            },
            sass: {
                files: ['assets/sass/**/*.scss'],
                tasks: ['sass', 'cssmin']
            }
            // ,
            // html: {
            //     files: ['/**/*.shtml'],
            //     tasks: ['ssi']
            // }
        }
    });
    var ssi = require("ssi");

    grunt.loadNpmTasks('grunt-ssi');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-usemin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    //grunt.loadNpmTasks('grunt-ssi');
    //grunt.loadNpmTasks('grunt-combine-media-queries');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-tinypng');

    grunt.loadNpmTasks('grunt-contrib-clean');


    grunt.registerTask('ssi', 'Flatten SSI includes in your HTML files.', function () {

        var ssi = require('ssi'),
            opts = this.options(),
            files = new ssi(opts.input, opts.output, opts.matcher);

        files.compile();

    });

    //'browserSync',
    grunt.registerTask('default', ['browserSync', 'watch']);
    // grunt.registerTask('build', ['sass', 'jshint', 'uglify', 'imagemin', 'postcss','uncss', 'uncss']);
    grunt.registerTask('build', ['ssi', 'copy', 'usemin', 'cssmin', 'uglify', 'clean']);
    // grunt.registerTask('default', ['jshint', 'browserSync', 'watch']); // from jade set up
    //grunt.registerTask('build', ['copy', 'useminPrepare', 'usemin', 'cssmin', 'uglify']);// from jade set up
}